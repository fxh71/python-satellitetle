CHANGELOG
=========

0.5.1
-----

- Reduce highest logging level from error to warning

0.5.0
-----

- Add 'Celestrak: active satellites' source, remove all 44 other Celestrak sources
- Add logging with different log levels


0.4.0
-----

- Add error handling to the fetch_tle/fetch_tles methods


0.3.1
-----

- Fix fetch_tles_from_url


0.3.0
-----

First release of python-satellitetle
